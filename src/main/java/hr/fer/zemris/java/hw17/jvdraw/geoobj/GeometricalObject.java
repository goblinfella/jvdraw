package hr.fer.zemris.java.hw17.jvdraw.geoobj;

import java.util.LinkedList;
import java.util.List;
import java.util.Objects;

/**
 * Abstract geometrical object
 * @author Fabijan
 *
 */
public abstract class GeometricalObject {
	/**
	 * Listeners
	 */
	private List<GeometricalObjectListener> listeners = new LinkedList<>();

	/**
	 * Accept visitor
	 * @param v
	 */
	public abstract void accept(GeometricalObjectVisitor v);

	/**
	 * Create the editor for object
	 * @return
	 */
	public abstract GeometricalObjectEditor createGeometricalObjectEditor();

	/**
	 * Register listener
	 * @param l
	 */
	public void addGeometricalObjectListener(GeometricalObjectListener l) {
		Objects.requireNonNull(l);
		listeners = new LinkedList<>(listeners);
		listeners.add(l);
	}

	/**
	 * Opposite of registering
	 * @param l
	 */
	public void removeGeometricalObjectListener(GeometricalObjectListener l) {
		Objects.requireNonNull(l);
		listeners = new LinkedList<>(listeners);
		listeners.remove(l);
	}
	
	/**
	 * Notify listeners
	 */
	public void fire() {
		listeners.forEach( l -> l.geometricalObjectChanged(this));
	}

}
