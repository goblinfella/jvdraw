package hr.fer.zemris.java.hw17.jvdraw.geoobj;

import java.awt.Graphics2D;
import java.awt.Point;
import java.awt.geom.Ellipse2D;

/**
 * Painter of a composite of geometrical objects
 * @author Fabijan
 *
 */
public class GeometricalObjectPainter implements GeometricalObjectVisitor {

	/**
	 * That which draws
	 */
	private Graphics2D g2d;

	/**
	 * Constructor
	 * @param g2d
	 */
	public GeometricalObjectPainter(Graphics2D g2d) {
		super();
		this.g2d = g2d;
	}

	@Override
	public void visit(Line line) {
		Point start = line.getStart();
		Point end = line.getEnd();
		g2d.setColor(line.getCol());
		g2d.drawLine(start.x, start.y, end.x, end.y);
	}

	@Override
	public void visit(Circle circle) {
		g2d.setColor(circle.getRimCol());
		g2d.draw(new Ellipse2D.Double(
					circle.getCenter().x - circle.getRadius(), 
					circle.getCenter().y - circle.getRadius(), 
					2*circle.getRadius(), 2*circle.getRadius())
				);
	}

	@Override
	public void visit(FilledCircle filledCircle) {
		g2d.setColor(filledCircle.getFillc());
		g2d.fill(new Ellipse2D.Double(
					filledCircle.getCenter().x - filledCircle.getRadius(), 
					filledCircle.getCenter().y - filledCircle.getRadius(), 
					2*filledCircle.getRadius(), 2*filledCircle.getRadius())
				);
		visit((Circle)filledCircle);
	}

}
