package hr.fer.zemris.java.hw17.jvdraw.state;

import java.awt.Graphics2D;
import java.awt.Point;
import java.awt.event.MouseEvent;

import hr.fer.zemris.java.hw17.jvdraw.DrawingModel;
import hr.fer.zemris.java.hw17.jvdraw.color.IColorProvider;

/**
 * Tool from which all other tools stem
 * @author Fabijan
 *
 */
public abstract class AbstractTool implements Tool {

	IColorProvider fg;

	IColorProvider bg;

	DrawingModel drMod;

	Point lstart = new Point(0, 0);

	Point lend = new Point(0, 0);

	public AbstractTool(IColorProvider fg, IColorProvider bg, DrawingModel drMod) {
		this.fg = fg;
		this.bg = bg;
		this.drMod = drMod;
	}

	@Override
	public void mousePressed(MouseEvent e) {
		lstart = e.getPoint();
	}

	@Override
	public void mouseReleased(MouseEvent e) {
		lend = e.getPoint();
	}

	@Override
	public void mouseClicked(MouseEvent e) {
	}

	@Override
	public void mouseMoved(MouseEvent e) {
	}

	@Override
	public void mouseDragged(MouseEvent e) {
	}

	@Override
	public void paint(Graphics2D g2d) {
	}

}
