package hr.fer.zemris.java.hw17.jvdraw;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.FlowLayout;
import java.awt.Graphics2D;
import java.awt.Point;
import java.awt.Rectangle;
import java.awt.event.ActionEvent;
import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.awt.image.BufferedImage;
import java.io.IOException;
import java.io.OutputStream;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.function.Supplier;

import javax.imageio.ImageIO;
import javax.swing.AbstractAction;
import javax.swing.Action;
import javax.swing.ButtonGroup;
import javax.swing.JColorChooser;
import javax.swing.JFileChooser;
import javax.swing.JFrame;
import javax.swing.JList;
import javax.swing.JMenu;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;
import javax.swing.JOptionPane;
import javax.swing.JScrollPane;
import javax.swing.JToolBar;
import javax.swing.SwingUtilities;

import hr.fer.zemris.java.hw17.jvdraw.color.ColorInfoLabel;
import hr.fer.zemris.java.hw17.jvdraw.color.IColorProvider;
import hr.fer.zemris.java.hw17.jvdraw.color.JColorArea;
import hr.fer.zemris.java.hw17.jvdraw.geoobj.Circle;
import hr.fer.zemris.java.hw17.jvdraw.geoobj.FilledCircle;
import hr.fer.zemris.java.hw17.jvdraw.geoobj.GeometricalObject;
import hr.fer.zemris.java.hw17.jvdraw.geoobj.GeometricalObjectBBCalculator;
import hr.fer.zemris.java.hw17.jvdraw.geoobj.GeometricalObjectEditor;
import hr.fer.zemris.java.hw17.jvdraw.geoobj.GeometricalObjectPainter;
import hr.fer.zemris.java.hw17.jvdraw.geoobj.Line;
import hr.fer.zemris.java.hw17.jvdraw.state.CircleTool;
import hr.fer.zemris.java.hw17.jvdraw.state.FilledCircleTool;
import hr.fer.zemris.java.hw17.jvdraw.state.LineTool;
import hr.fer.zemris.java.hw17.jvdraw.state.StateButton;
import hr.fer.zemris.java.hw17.jvdraw.state.Tool;

/**
 * Class that serves as the entry point for a super cool drawing app
 * @author Fabijan
 *
 */
@SuppressWarnings("serial")
public class JVDraw extends JFrame implements Supplier<Tool> {

	/**
	 * Drawing model
	 */
	private DrawingModel dmodel;

	/**
	 * Foreground color provider
	 */
	private IColorProvider fgcp;

	/**
	 * Background color provider
	 */
	private IColorProvider bgcp;

	/**
	 * Current tool used
	 */
	private Tool currentState;

	/**
	 * Path to which to save
	 */
	private Path file;

	private static final Color DEF_FG_COL = Color.BLACK;

	private static final Color DEF_BG_COL = Color.WHITE;

	/**
	 * Constructor
	 */
	public JVDraw() {
		setDefaultCloseOperation(DO_NOTHING_ON_CLOSE);
		setLocation(10, 10);
		setSize(700, 500);
		setTitle("JVDraw");

		initGUI();
	}

	/**
	 * Create all components
	 */
	private void initGUI() {
		dmodel = new DrawingModelImpl();
		setLayout(new BorderLayout());

		createActions();

		setJMenuBar(createMenu());
		add(createToolbar(), BorderLayout.NORTH);
		add(new JScrollPane(createJList()), BorderLayout.EAST);

		add(new JDrawingCanvas(dmodel, this));
		add(new ColorInfoLabel(fgcp, bgcp), BorderLayout.SOUTH);

		addWindowListener(new WindowAdapter() {
			@Override
			public void windowClosing(WindowEvent e) {
				exitAc.actionPerformed(null);
			}
		});
	}

	/**
	 * Give names to actions
	 */
	private void createActions() {
		exitAc.putValue(Action.NAME, "exit");
		saveAc.putValue(Action.NAME, "save");
		saveasAc.putValue(Action.NAME, "save as");
		openAc.putValue(Action.NAME, "open");
		exportAc.putValue(Action.NAME, "export");
	}

	/**
	 * Create the menu
	 * @return the menu
	 */
	private JMenuBar createMenu() {
		JMenuBar mbar = new JMenuBar();
		JMenu file = new JMenu("file");
		JMenuItem open = new JMenuItem(openAc);
		JMenuItem save = new JMenuItem(saveAc);
		JMenuItem saveas = new JMenuItem(saveasAc);
		JMenuItem export = new JMenuItem(exportAc);
		JMenuItem exit = new JMenuItem(exitAc);
		file.add(open);
		file.add(save);
		file.add(saveas);
		file.add(export);
		file.add(exit);
		mbar.add(file);
		return mbar;
	}

	/**
	 * Create the toolbar
	 * @return the toolbar
	 */
	private JToolBar createToolbar() {
		JToolBar jtb = new JToolBar();
		jtb.setLayout(new FlowLayout());
		JColorArea fgca = new JColorArea(DEF_FG_COL);
		fgcp = fgca;
		fgca.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent e) {
				fgca.setCurrentColor(JColorChooser.showDialog(fgca, "Choose color", fgca.getCurrentColor()));
			}
		});
		JColorArea bgca = new JColorArea(DEF_BG_COL);
		bgcp = bgca;
		bgca.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent e) {
				bgca.setCurrentColor(JColorChooser.showDialog(bgca, "Choose color", bgca.getCurrentColor()));
			}
		});
		jtb.add(fgca, FlowLayout.LEFT);
		jtb.add(bgca);

		ButtonGroup bg = new ButtonGroup();

		currentState = new LineTool(fgcp, bgcp, dmodel);
		StateButton line = new StateButton("line", currentState, this);
		line.setSelected(true);
		bg.add(line);
		StateButton circle = new StateButton("circle", new CircleTool(fgcp, bgcp, dmodel), this);
		bg.add(circle);
		StateButton filledcircle = new StateButton("filled circle", new FilledCircleTool(fgcp, bgcp, dmodel), this);
		bg.add(filledcircle);

		jtb.add(line);
		jtb.add(circle);
		jtb.add(filledcircle);
		return jtb;
	}

	/**
	 * Create the list 
	 * @return the list
	 */
	private JList<GeometricalObject> createJList() {
		DrawingObjectListModel golm = new DrawingObjectListModel(dmodel);
		JList<GeometricalObject> jlist = new JList<>(golm);
		jlist.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent e) {
				if (e.getClickCount() == 2) {
					int index = jlist.locationToIndex(e.getPoint());
					GeometricalObjectEditor goe = jlist.getModel().getElementAt(index).createGeometricalObjectEditor();
					JOptionPane.showConfirmDialog(JVDraw.this, goe);
					try {
						goe.checkEditing();
						goe.acceptEditing();
					} catch (Exception ex) {
						JOptionPane.showMessageDialog(JVDraw.this, ex.getMessage());
					}
				}
			}
		});
		jlist.addKeyListener(new KeyAdapter() {
			@Override
			public void keyTyped(KeyEvent e) {
				GeometricalObject geob = jlist.getSelectedValue();
				switch (e.getKeyChar()) {
				case KeyEvent.VK_MINUS:
					dmodel.changeOrder(geob, -1);
					break;
				case '+':
					dmodel.changeOrder(geob, +1);
					break;
				case KeyEvent.VK_DELETE:
					dmodel.remove(geob);
					break;
				}
			}
		});
		return jlist;
	}

	/**
	 * Set the current tool/state
	 * @param state
	 */
	public void setState(Tool state) {
		this.currentState = state;
	}

	/**
	 * Entry point, takes nothing
	 * @param args
	 */
	public static void main(String[] args) {
		SwingUtilities.invokeLater(() -> new JVDraw().setVisible(true));
	}

	@Override
	public Tool get() {
		return currentState;
	}

	/**
	 * Exit the app
	 */
	private Action exitAc = new AbstractAction() {
		@Override
		public void actionPerformed(ActionEvent e) {
			if (dmodel.isModified()) {
				int chosen = JOptionPane.showConfirmDialog(JVDraw.this, "You have unsaved work. Save?",
						"Close jv draw?", JOptionPane.OK_OPTION);
				switch (chosen) {
				case JOptionPane.OK_OPTION:
					saveAc.actionPerformed(null);
					break;
				case JOptionPane.NO_OPTION:
					JVDraw.this.dispose();
					break;
				case JOptionPane.CANCEL_OPTION:
					break;
				}
			} else {
				JVDraw.this.dispose();
			}
		}
	};

	/**
	 * Save current work
	 */
	private Action saveAc = new AbstractAction() {
		@Override
		public void actionPerformed(ActionEvent e) {
			if (file == null) {
				saveasAc.actionPerformed(null);
			} else {
				save();
			}
		}
	};

	/**
	 * Method for saving current work
	 */
	private void save() {
		try (OutputStream os = Files.newOutputStream(file)) {
			for (int i = 0; i < dmodel.getSize(); i++) {
				os.write(dmodel.getObject(i).toString().getBytes());
				os.write('\n');
			}
			os.flush();
		} catch (IOException e1) {
			e1.printStackTrace();
		}
		dmodel.clearModifiedFlag();
	}

	/**
	 * Save current work under name
	 */
	private Action saveasAc = new AbstractAction() {
		@Override
		public void actionPerformed(ActionEvent e) {
			JFileChooser jfc = new JFileChooser();
			jfc.setDialogTitle("Save to file");
			if (jfc.showSaveDialog(JVDraw.this) != JFileChooser.APPROVE_OPTION) {
				JOptionPane.showMessageDialog(JVDraw.this, "File not saved", "Save file",
						JOptionPane.INFORMATION_MESSAGE);
				return;
			}
			file = jfc.getSelectedFile().toPath();
			save();
		}
	};

	/**
	 * Open new file
	 */
	private Action openAc = new AbstractAction() {
		@Override
		public void actionPerformed(ActionEvent e) {
			if (dmodel.isModified()) {
				int chosen = JOptionPane.showConfirmDialog(JVDraw.this, "You have unsaved work. Save?",
						"Save work before opening new file?", JOptionPane.OK_OPTION);
				switch (chosen) {
				case JOptionPane.OK_OPTION:
					saveAc.actionPerformed(null);
					break;
				case JOptionPane.NO_OPTION:
					break;
				case JOptionPane.CANCEL_OPTION:
					return;
				}
			}
			JFileChooser jfc = new JFileChooser();
			jfc.setDialogTitle("Open file");
			if (jfc.showOpenDialog(JVDraw.this) != JFileChooser.APPROVE_OPTION) {
				return;
			}
			
			if (!jfc.getSelectedFile().toString().endsWith("jvd")) {
				JOptionPane.showMessageDialog(JVDraw.this, "Wrong file format.");
				return;
			}

			file = jfc.getSelectedFile().toPath();
			dmodel.clear();
			try {
				Files.readAllLines(file).forEach(line -> {
					String name = line.split("\\s+")[0];
					line = line.replaceFirst(name, "");
					String[] parts = line.trim().split("\\s+");
					GeometricalObject geob = null;
					switch (name) {
					case "LINE":
						geob = new Line(
								new Point(
										Integer.parseInt(parts[0]),
										Integer.parseInt(parts[1])
										), 
									new Point(
											Integer.parseInt(parts[2]),
											Integer.parseInt(parts[3])
											), 
									new Color(
											Integer.parseInt(parts[4]),
											Integer.parseInt(parts[5]),
											Integer.parseInt(parts[6])
									)
							);
						break;
					case "CIRCLE":
						geob = new Circle(
								new Point(
										Integer.parseInt(parts[0]),
										Integer.parseInt(parts[1])
										), 
									Integer.parseInt(parts[2]), 
									new Color(
											Integer.parseInt(parts[3]),
											Integer.parseInt(parts[4]),
											Integer.parseInt(parts[5])
									)
							);
						break;
					case "FCIRCLE":
						geob = new FilledCircle(
								new Point(
										Integer.parseInt(parts[0]),
										Integer.parseInt(parts[1])
										), 
									Integer.parseInt(parts[2]), 
									new Color(
											Integer.parseInt(parts[3]),
											Integer.parseInt(parts[4]),
											Integer.parseInt(parts[5])
									),
									new Color(
											Integer.parseInt(parts[6]),
											Integer.parseInt(parts[7]),
											Integer.parseInt(parts[8])
									)
							);
						break;
					default: throw new IllegalArgumentException();
					}
					dmodel.add(geob);
				});
			} catch (IOException e1) {
				e1.printStackTrace();
			}
			dmodel.clearModifiedFlag();
		}
	};
	
	/**
	 * Export as png/jpg/gif
	 */
	private Action exportAc = new AbstractAction() {
		@Override
		public void actionPerformed(ActionEvent e) {
			JFileChooser jfc = new JFileChooser();
			jfc.setDialogTitle("Choose png, jpg or gif");
			if (jfc.showSaveDialog(JVDraw.this) != JFileChooser.APPROVE_OPTION) {
				JOptionPane.showMessageDialog(JVDraw.this, "File not exported", "export file",
						JOptionPane.INFORMATION_MESSAGE);
				return;
			}
			Path path = jfc.getSelectedFile().toPath();
			Path filename = path.getFileName();
			String fname = filename.toString();
			if (!fname.endsWith(".png") && 
				!fname.endsWith(".jpg") && 
				!fname.endsWith(".gif")) {
				JOptionPane.showMessageDialog(JVDraw.this, "Wrong format.");
				return;
			} else {
				String[] parts = path.toString().split("\\.");
				String extension = parts[parts.length - 1];
				GeometricalObjectBBCalculator bbcal = new GeometricalObjectBBCalculator();
				for (int i = 0; i < dmodel.getSize(); i++) {
					dmodel.getObject(i).accept(bbcal);
				}
				Rectangle bb = bbcal.getBoundingBox();
				BufferedImage bim = new BufferedImage(bb.width, bb.height, BufferedImage.TYPE_3BYTE_BGR);
				Graphics2D g = bim.createGraphics();
				GeometricalObjectPainter gop = new GeometricalObjectPainter(g);
				for (int i = 0; i < dmodel.getSize(); i++) {
					dmodel.getObject(i).accept(gop);
				}
				g.dispose();
				try {
					ImageIO.write(bim, extension, Files.newOutputStream(path));
					JOptionPane.showMessageDialog(JVDraw.this, "Exported");
				} catch (IOException e1) {
					e1.printStackTrace();
				}
			}
		}
	};

}
