package hr.fer.zemris.java.hw17.jvdraw.color;

import java.awt.Color;

/**
 * Listen for the changing color
 * @author Fabijan
 *
 */
public interface ColorChangeListener {
	
	/**
	 * The color has changed
	 * @param source
	 * @param oldColor
	 * @param newColor
	 */
	public void newColorSelected(IColorProvider source, Color oldColor, Color newColor);

}
