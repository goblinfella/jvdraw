package hr.fer.zemris.java.hw17.jvdraw;

/**
 * Interface for listeners of {@link DrawingModel}
 * @author Fabijan
 *
 */
public interface DrawingModelListener {

	/**
	 * Object added to index
	 * @param source
	 * @param index0
	 * @param index1
	 */
	public void objectsAdded(DrawingModel source, int index0, int index1);

	/**
	 * Object removed from index
	 * @param source
	 * @param index0
	 * @param index1
	 */
	public void objectsRemoved(DrawingModel source, int index0, int index1);

	/**
	 * Object moved places
	 * @param source
	 * @param index0
	 * @param index1
	 */
	public void objectsChanged(DrawingModel source, int index0, int index1);

}
