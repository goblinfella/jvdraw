package hr.fer.zemris.java.hw17.jvdraw;

import hr.fer.zemris.java.hw17.jvdraw.geoobj.GeometricalObject;

/**
 * Model for vector graphics
 * @author Fabijan
 *
 */
public interface DrawingModel {
	
	/**
	 * Number of objects
	 * @return
	 */
	public int getSize();

	/**
	 * Object at index
	 * @param index
	 * @return
	 */
	public GeometricalObject getObject(int index);

	/**
	 * Add object
	 * @param object
	 */
	public void add(GeometricalObject object);

	/*
	 * Remove object
	 */
	public void remove(GeometricalObject object);

	/**
	 * Change order in objects, for overlapping
	 * @param object
	 * @param offset
	 */
	public void changeOrder(GeometricalObject object, int offset);

	/**
	 * Index of a object
	 * @param object
	 * @return
	 */
	public int indexOf(GeometricalObject object);

	/**
	 * Remove all objects
	 */
	public void clear();

	/**
	 * Reset modification flag
	 */
	public void clearModifiedFlag();

	/**
	 * Has the model been modified
	 * @return
	 */
	public boolean isModified();

	/**
	 * add listener
	 * @param l
	 */
	public void addDrawingModelListener(DrawingModelListener l);

	/**
	 * remove listener
	 * @param l
	 */
	public void removeDrawingModelListener(DrawingModelListener l);
}
