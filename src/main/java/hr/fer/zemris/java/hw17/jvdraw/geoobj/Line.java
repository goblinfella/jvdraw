package hr.fer.zemris.java.hw17.jvdraw.geoobj;

import java.awt.Color;
import java.awt.Point;

/**
 * Line, it is the line
 * @author Fabijan
 *
 */
public class Line extends GeometricalObject {

	private Point start;

	private Point end;

	private Color col;

	public Line(Point start, Point end, Color col) {
		super();
		this.start = start;
		this.end = end;
		this.col = col;
	}

	@Override
	public void accept(GeometricalObjectVisitor v) {
		v.visit(this);
	}

	public Point getStart() {
		return start;
	}

	public void setStart(Point start) {
		this.start = start;
	}

	public Point getEnd() {
		return end;
	}

	public void setEnd(Point end) {
		this.end = end;
	}

	public Color getCol() {
		return col;
	}

	public void setCol(Color col) {
		this.col = col;
	}

	@Override
	public GeometricalObjectEditor createGeometricalObjectEditor() {
		return new LineEditor(this);
	}

	@Override
	public String toString() {
		return String.format("LINE %d %d %d %d %d %d %d",
				start.x, start.y, 
				end.x, end.y, 
				col.getRed(), col.getGreen(), col.getBlue());
	}

}
