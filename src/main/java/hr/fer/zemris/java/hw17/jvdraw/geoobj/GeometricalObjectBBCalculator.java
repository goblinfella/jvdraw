package hr.fer.zemris.java.hw17.jvdraw.geoobj;

import java.awt.Point;
import java.awt.Rectangle;

/**
 * Calculator for bounding box
 * @author Fabijan Džapo
 *
 */
public class GeometricalObjectBBCalculator implements GeometricalObjectVisitor {
	
	int minx;
	int miny;
	int maxx;
	int maxy;

	@Override
	public void visit(Line line) {
		Point start = line.getStart();
		Point end = line.getEnd();
		minx = Math.min(minx, Math.min(start.x, end.x));
		maxx = Math.max(maxx, Math.max(start.x, end.x));
		miny = Math.min(miny, Math.min(start.y, end.y));
		maxy = Math.max(maxy, Math.max(start.y, end.y));
	}

	@Override
	public void visit(Circle circle) {
		Point center = circle.getCenter();
		double radius = circle.getRadius();
		minx = (int) Math.min(minx, center.x - radius);
		maxx = (int) Math.max(maxx, center.x + radius);
		miny = (int) Math.min(miny, center.y - radius);
		maxy = (int) Math.max(maxy, center.y + radius);
	}

	@Override
	public void visit(FilledCircle filledCircle) {
		visit((Circle)filledCircle);
	}
	
	public Rectangle getBoundingBox() {
		return new Rectangle(minx, miny, maxx - minx, maxy - miny);
	}

}
