package hr.fer.zemris.java.hw17.jvdraw.geoobj;

/**
 * Visitor of geometrical objects
 * @author Fabijan
 *
 */
public interface GeometricalObjectVisitor {

	/**
	 * Visit circle
	 * @param line
	 */
	public abstract void visit(Line line);

	/**
	 * Visit circle
	 * @param circle
	 */
	public abstract void visit(Circle circle);

	/**
	 * Visit filled circle
	 * @param filledCircle
	 */
	public abstract void visit(FilledCircle filledCircle);

}
