package hr.fer.zemris.java.hw17.jvdraw.geoobj;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.Point;

import javax.swing.JLabel;
import javax.swing.JTextField;

/**
 * Editor for the line
 * @author Fabijan
 *
 */
@SuppressWarnings("serial")
public class LineEditor extends GeometricalObjectEditor {

	private Line line;

	private JLabel lx1 = new JLabel("x1");

	private JLabel lx2 = new JLabel("x2");

	private JLabel ly1 = new JLabel("y1");

	private JLabel ly2 = new JLabel("y2");

	private JLabel lred = new JLabel("red");

	private JLabel lgreen = new JLabel("green");

	private JLabel lblue = new JLabel("blue");

	private JTextField x1 = new JTextField();

	private JTextField x2 = new JTextField();

	private JTextField y1 = new JTextField();

	private JTextField y2 = new JTextField();

	private JTextField red = new JTextField();

	private JTextField green = new JTextField();

	private JTextField blue = new JTextField();

	/**
	 * @param line
	 */
	public LineEditor(Line line) {
		super();
		this.line = line;
		add(lx1);
		add(x1);
		x1.setPreferredSize(new Dimension(40, 20));
		add(ly1);
		add(y1);
		y1.setPreferredSize(new Dimension(40, 20));

		add(lx2);
		add(x2);
		x2.setPreferredSize(new Dimension(40, 20));
		add(ly2);
		add(y2);
		y2.setPreferredSize(new Dimension(40, 20));

		add(lred);
		add(red);
		red.setPreferredSize(new Dimension(40, 20));
		add(lgreen);
		add(green);
		green.setPreferredSize(new Dimension(40, 20));
		add(lblue);
		add(blue);
		blue.setPreferredSize(new Dimension(40, 20));
	}

	@Override
	public void checkEditing() throws Exception {
		if (!EditorUtil.isInt(x1) ||
			!EditorUtil.isInt(x2) ||
			!EditorUtil.isInt(y1) ||
			!EditorUtil.isInt(y2) ||
			!EditorUtil.isColor(red) ||
			!EditorUtil.isColor(green) ||
			!EditorUtil.isColor(blue)) {
			throw new Exception("Invalid field");
		}
	}

	@Override
	public void acceptEditing() {
		int px1 = EditorUtil.isEmpty(x1) ? line.getStart().x : Integer.parseInt(x1.getText());
		int px2 = EditorUtil.isEmpty(x2) ? line.getEnd().x : Integer.parseInt(x2.getText());
		int py1 = EditorUtil.isEmpty(y1) ? line.getStart().y : Integer.parseInt(y1.getText());
		int py2 = EditorUtil.isEmpty(y2) ? line.getEnd().y : Integer.parseInt(y2.getText());
		int cred = red.getText().isEmpty() ? line.getCol().getRed() : Integer.parseInt(red.getText());
		int cgreen = green.getText().isEmpty() ? line.getCol().getGreen() : Integer.parseInt(green.getText());
		int cblue = blue.getText().isEmpty() ? line.getCol().getBlue() : Integer.parseInt(blue.getText());
		line.setStart(new Point(px1, py1));
		line.setEnd(new Point(px2, py2));
		line.setCol(new Color(cred, cgreen, cblue));
		line.fire();
	}

}
