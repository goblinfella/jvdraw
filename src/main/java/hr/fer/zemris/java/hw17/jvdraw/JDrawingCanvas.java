package hr.fer.zemris.java.hw17.jvdraw;

import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.util.function.Supplier;

import javax.swing.JComponent;

import hr.fer.zemris.java.hw17.jvdraw.geoobj.GeometricalObjectPainter;
import hr.fer.zemris.java.hw17.jvdraw.state.Tool;

/**
 * JComponent used as canvas for drawing
 * @author Fabijan
 *
 */
@SuppressWarnings("serial")
public class JDrawingCanvas extends JComponent implements DrawingModelListener {

	/**
	 * Drawing model, source for painting
	 */
	private DrawingModel dmod;

	/**
	 * Supplier of tool that creates new objects
	 */
	private Supplier<Tool> toolSup;

	/**
	 * @param dmod
	 */
	public JDrawingCanvas(DrawingModel dmod, Supplier<Tool> toolSup) {
		super();
		this.dmod = dmod;
		dmod.addDrawingModelListener(this);
		this.toolSup = toolSup;
		addMouseListener(new MouseAdapter() {
			@Override
			public void mouseReleased(MouseEvent e) {
				toolSup.get().mouseReleased(e);
			}

			@Override
			public void mousePressed(MouseEvent e) {
				toolSup.get().mousePressed(e);
			}

			@Override
			public void mouseMoved(MouseEvent e) {
				toolSup.get().mouseMoved(e);
			}

			@Override
			public void mouseClicked(MouseEvent e) {
				toolSup.get().mouseClicked(e);
			}

			@Override
			public void mouseDragged(MouseEvent e) {
				toolSup.get().mouseDragged(e);
			}
		});
	}

	@Override
	public void objectsAdded(DrawingModel source, int index0, int index1) {
		repaint();
	}

	@Override
	public void objectsRemoved(DrawingModel source, int index0, int index1) {
		repaint();
	}

	@Override
	public void objectsChanged(DrawingModel source, int index0, int index1) {
		repaint();
	}
	
	@Override
	protected void paintComponent(Graphics g) {
		GeometricalObjectPainter geobPainter = new GeometricalObjectPainter((Graphics2D) g);
		for (int i = 0; i < dmod.getSize(); i++) {
			dmod.getObject(i).accept(geobPainter);
		}
		toolSup.get().paint((Graphics2D) g);
		g.dispose();
	}

}
