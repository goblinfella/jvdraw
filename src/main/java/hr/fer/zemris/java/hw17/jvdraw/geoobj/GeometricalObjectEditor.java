package hr.fer.zemris.java.hw17.jvdraw.geoobj;

import javax.swing.JPanel;

/**
 * Editor of geometrical object, it will edit an object under conditions...
 * @author Fabijan
 *
 */
@SuppressWarnings("serial")
public abstract class GeometricalObjectEditor extends JPanel {

	/**
	 * Are the changes valid? If so cool
	 * @throws Exception
	 */
	public abstract void checkEditing() throws Exception;

	/**
	 * Register the changes
	 */
	public abstract void acceptEditing();

}
