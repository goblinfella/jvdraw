package hr.fer.zemris.java.hw17.jvdraw.color;

import java.awt.Color;

import javax.swing.JLabel;

/**
 * Label displaying current colors
 * @author Fabijan
 *
 */
@SuppressWarnings("serial")
public class ColorInfoLabel extends JLabel implements ColorChangeListener {
	/**
	 * Foreground color provider
	 */
	private IColorProvider fgColP;

	/**
	 * Background color provider
	 */
	private IColorProvider bgColP;

	/**
	 * Constructor
	 * @param fgColP
	 * @param bgColP
	 */
	public ColorInfoLabel(IColorProvider fgColP, IColorProvider bgColP) {
		super();
		this.fgColP = fgColP;
		this.bgColP = bgColP;
		fgColP.addColorChangeListener(this);
		bgColP.addColorChangeListener(this);
		newColorSelected(null, null, null);
		setBackground(Color.DARK_GRAY);
	}

	@Override
	public void newColorSelected(IColorProvider source, Color oldColor, Color newColor) {
		Color forColor = fgColP.getCurrentColor();
		Color bacColor = bgColP.getCurrentColor();
		setText(String.format(
					"Foreground color: (%d, %d, %d) Background Color: (%d, %d, %d)", 
					forColor.getRed(), forColor.getGreen(), forColor.getBlue(),
					bacColor.getRed(), bacColor.getGreen(), bacColor.getBlue())
				);
	}

}
