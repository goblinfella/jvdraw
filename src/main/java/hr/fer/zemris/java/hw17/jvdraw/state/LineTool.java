package hr.fer.zemris.java.hw17.jvdraw.state;

import java.awt.Graphics2D;
import java.awt.Point;
import java.awt.event.MouseEvent;

import hr.fer.zemris.java.hw17.jvdraw.DrawingModel;
import hr.fer.zemris.java.hw17.jvdraw.color.IColorProvider;
import hr.fer.zemris.java.hw17.jvdraw.geoobj.Line;

/**
 * Tool, it creates the line
 * @author Fabijan
 *
 */
public class LineTool extends AbstractTool {
	
	public LineTool(IColorProvider fg, IColorProvider bg, DrawingModel drMod) {
		super(fg, bg, drMod);
	}
	
	@Override
	public void mouseReleased(MouseEvent e) {
		super.mouseReleased(e);
		drMod.add(new Line(new Point(lstart.x, lstart.y), new Point(lend.x, lend.y), fg.getCurrentColor()));
	}

	@Override
	public void paint(Graphics2D g2d) {
		g2d.setColor(fg.getCurrentColor());
		g2d.drawLine(lstart.x, lstart.y, lstart.x, lstart.y);
	}

}
