package hr.fer.zemris.java.hw17.jvdraw.geoobj;

import java.awt.Color;
import java.awt.Point;

/**
 * Circle
 * @author Fabijan
 *
 */
public class Circle extends GeometricalObject {

	/**
	 * Center
	 */
	protected Point center;

	/**
	 * Distance from center to all points
	 */
	protected double radius;
	
	/**
	 * Color of rim
	 */
	protected Color rimCol;

	/**
	 * Constructor
	 * @param center
	 * @param radius
	 * @param col
	 */
	public Circle(Point center, double radius, Color col) {
		super();
		this.center = center;
		this.radius = radius;
		this.rimCol = col;
	}

	@Override
	public void accept(GeometricalObjectVisitor v) {
		v.visit(this);
	}

	public Point getCenter() {
		return center;
	}

	public void setCenter(Point center) {
		this.center = center;
	}

	public double getRadius() {
		return radius;
	}

	public void setRadius(double radius) {
		this.radius = radius;
	}

	public Color getRimCol() {
		return rimCol;
	}

	public void setRimCol(Color col) {
		this.rimCol = col;
	}

	@Override
	public GeometricalObjectEditor createGeometricalObjectEditor() {
		return new CircleEditor(this);
	}
	
	@Override
	public String toString() {
		return String.format("CIRCLE %d %d %d %d %d %d", 
				center.x, center.y,
				(int) radius,
				rimCol.getRed(), rimCol.getGreen(), rimCol.getBlue());
	}

}
