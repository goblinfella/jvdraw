package hr.fer.zemris.java.hw17.jvdraw.state;

import java.awt.Point;
import java.awt.event.MouseEvent;

import hr.fer.zemris.java.hw17.jvdraw.DrawingModel;
import hr.fer.zemris.java.hw17.jvdraw.color.IColorProvider;
import hr.fer.zemris.java.hw17.jvdraw.geoobj.FilledCircle;

/**
 * Tool that creates the filled circle
 * @author Fabijan
 *
 */
public class FilledCircleTool extends AbstractTool {

	public FilledCircleTool(IColorProvider fg, IColorProvider bg, DrawingModel drMod) {
		super(fg, bg, drMod);
	}
	
	@Override
	public void mouseReleased(MouseEvent e) {
		super.mouseReleased(e);
		double radius = Math.hypot(lend.x - lstart.x, lend.y - lstart.y);
		drMod.add(new FilledCircle(new Point(lstart.x, lstart.y), radius, fg.getCurrentColor(), bg.getCurrentColor()));
	}

}
