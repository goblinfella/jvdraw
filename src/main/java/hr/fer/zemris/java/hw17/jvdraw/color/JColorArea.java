package hr.fer.zemris.java.hw17.jvdraw.color;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.Graphics;
import java.util.LinkedList;
import java.util.List;
import java.util.Objects;

import javax.swing.JComponent;

@SuppressWarnings("serial")
public class JColorArea extends JComponent 
	implements IColorProvider{
	
	private Color selectedColor;
	
	private List<ColorChangeListener> colChList = new LinkedList<>();
	
	public JColorArea(Color selectedColor) {
		super();
		this.selectedColor = selectedColor;
	}
	
	@Override
	public Dimension getPreferredSize() {
		return new Dimension(15, 15);
	}

	@Override
	public Color getCurrentColor() {
		return selectedColor;
	}
	
	public void setCurrentColor(Color color) {
		Color oldc = selectedColor;
		this.selectedColor = color;
		notifyColChLs(oldc, color);
		repaint();
	}

	@Override
	public void addColorChangeListener(ColorChangeListener l) {
		Objects.requireNonNull(l);
		colChList = new LinkedList<ColorChangeListener>(colChList);
		colChList.add(l);
	}

	@Override
	public void removeColorChangeListener(ColorChangeListener l) {
		Objects.requireNonNull(l);
		colChList = new LinkedList<ColorChangeListener>(colChList);
		colChList.remove(l);
	}
	
	private void notifyColChLs(Color oldColor, Color newColor) {
		colChList.forEach( l -> l.newColorSelected(this, oldColor, newColor));
	}
	
	@Override
	public void paint(Graphics g) {
		g.setColor(selectedColor);
		g.fillRect(0, 0, getWidth(), getHeight());
	}

}
