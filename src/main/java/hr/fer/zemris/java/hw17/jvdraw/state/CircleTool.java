package hr.fer.zemris.java.hw17.jvdraw.state;

import java.awt.Graphics2D;
import java.awt.Point;
import java.awt.event.MouseEvent;
import java.awt.geom.Ellipse2D;

import hr.fer.zemris.java.hw17.jvdraw.DrawingModel;
import hr.fer.zemris.java.hw17.jvdraw.color.IColorProvider;
import hr.fer.zemris.java.hw17.jvdraw.geoobj.Circle;

/**
 * Tool for the construction of a circle
 * @author Fabijan
 *
 */
public class CircleTool extends AbstractTool {

	public CircleTool(IColorProvider fg, IColorProvider bg, DrawingModel drMod) {
		super(fg, bg, drMod);
	}
	
	@Override
	public void mouseReleased(MouseEvent e) {
		super.mouseReleased(e);
		double radius = Math.hypot(lend.x - lstart.x, lend.y - lstart.y);
		drMod.add(new Circle(new Point(lstart.x, lstart.y), radius, fg.getCurrentColor()));
	}

	@Override
	public void paint(Graphics2D g2d) {
		g2d.setColor(fg.getCurrentColor());
		double radius = Math.hypot(lend.x - lstart.x, lend.y - lstart.y);
		g2d.draw(new Ellipse2D.Double(lstart.x -radius, lstart.y - radius, 2*radius, 2*radius));
	}

}
