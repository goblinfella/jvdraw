package hr.fer.zemris.java.hw17.jvdraw.geoobj;

import java.awt.Color;
import java.awt.Point;

/**
 * Circle with filling
 * @author Fabijan
 *
 */
public class FilledCircle extends Circle {

	/**
	 * Color on the inside
	 */
	private Color fillc;

	public FilledCircle(Point center, double radius, Color rimcolor, Color fillcol) {
		super(center, radius, rimcolor);
		this.fillc = fillcol;
	}

	public Color getFillc() {
		return fillc;
	}

	public void setFillc(Color fillc) {
		this.fillc = fillc;
	}

	@Override
	public void accept(GeometricalObjectVisitor v) {
		v.visit(this);
	}

	@Override
	public GeometricalObjectEditor createGeometricalObjectEditor() {
		return new FilledCircleEditor(this);
	}
	
	public String toString() {
		return String.format("FCIRCLE %d %d %d %d %d %d %d %d %d", 
				center.x, center.y,
				(int) radius,
				rimCol.getRed(), rimCol.getGreen(), rimCol.getBlue(),
				fillc.getRed(), fillc.getGreen(), fillc.getBlue());
	}

}
