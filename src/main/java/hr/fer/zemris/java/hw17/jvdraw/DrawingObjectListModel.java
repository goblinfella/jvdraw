package hr.fer.zemris.java.hw17.jvdraw;

import javax.swing.AbstractListModel;

import hr.fer.zemris.java.hw17.jvdraw.geoobj.GeometricalObject;

/**
 * Model for a list of objects from {@link DrawingModel}
 * @author Fabijan
 *
 */
@SuppressWarnings("serial")
public class DrawingObjectListModel extends AbstractListModel<GeometricalObject> implements DrawingModelListener {

	/**
	 * Drawing model, source
	 */
	private DrawingModel dmod;

	/**
	 * @param dmod
	 */
	DrawingObjectListModel(DrawingModel dmod) {
		super();
		this.dmod = dmod;
		dmod.addDrawingModelListener(this);
	}

	@Override
	public int getSize() {
		return dmod.getSize();
	}

	@Override
	public GeometricalObject getElementAt(int index) {
		return dmod.getObject(index);
	}

	@Override
	public void objectsAdded(DrawingModel source, int index0, int index1) {
		fireIntervalAdded(this, index0, index1);
	}

	@Override
	public void objectsRemoved(DrawingModel source, int index0, int index1) {
		fireIntervalRemoved(this, index0, index1);
	}

	@Override
	public void objectsChanged(DrawingModel source, int index0, int index1) {
		fireContentsChanged(this, index0, index1);
	}

}
