package hr.fer.zemris.java.hw17.jvdraw.state;

import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;

import javax.swing.JToggleButton;

import hr.fer.zemris.java.hw17.jvdraw.JVDraw;

/**
 * Button that changes the state (tool)
 * @author Fabijan
 *
 */
@SuppressWarnings("serial")
public class StateButton extends JToggleButton {

	private Tool tool;
	
	private JVDraw parent;

	/**
	 * @param tool
	 */
	public StateButton(String name, Tool tool, JVDraw parent) {
		super(name);
		this.tool = tool;
		this.parent = parent;
		this.addMouseListener(moulis);
	}

	private MouseListener moulis = new MouseAdapter() {
		public void mouseClicked(MouseEvent e) {
			parent.setState(tool);
		};
	};

}
