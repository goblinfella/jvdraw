package hr.fer.zemris.java.hw17.jvdraw.geoobj;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.Point;

import javax.swing.JLabel;
import javax.swing.JTextField;

/**
 * Editor for the filled circle
 * @author Fabijan
 *
 */
@SuppressWarnings("serial")
public class FilledCircleEditor extends GeometricalObjectEditor {

	private FilledCircle fcircle;
	
	private JLabel lx1 = new JLabel("x1");

	private JLabel ly1 = new JLabel("y1");
	
	private JLabel lradius = new JLabel("radius");

	private JLabel lrred = new JLabel("rim red");

	private JLabel lrgreen = new JLabel("rim green");

	private JLabel lrblue = new JLabel("rim blue");

	private JTextField x1 = new JTextField();

	private JTextField y1 = new JTextField();
	
	private JTextField radius = new JTextField();

	private JTextField rred = new JTextField();

	private JTextField rgreen = new JTextField();

	private JTextField rblue = new JTextField();

	private JLabel lfred = new JLabel("fill red");

	private JLabel lfgreen = new JLabel("fill green");

	private JLabel lfblue = new JLabel("fill blue");

	private JTextField fred = new JTextField();

	private JTextField fgreen = new JTextField();

	private JTextField fblue = new JTextField();

	/**
	 * @param fcircle
	 */
	public FilledCircleEditor(FilledCircle fcircle) {
		this.fcircle = fcircle;
		add(lx1);
		add(x1);
		x1.setPreferredSize(new Dimension(40, 20));
		add(ly1);
		add(y1);
		y1.setPreferredSize(new Dimension(40, 20));
		
		add(lradius);
		add(radius);
		radius.setPreferredSize(new Dimension(40, 20));

		add(lrred);
		add(rred);
		rred.setPreferredSize(new Dimension(40, 20));
		add(lrgreen);
		add(rgreen);
		rgreen.setPreferredSize(new Dimension(40, 20));
		add(lrblue);
		add(rblue);
		rblue.setPreferredSize(new Dimension(40, 20));
		
		add(lfred);
		add(fred);
		fred.setPreferredSize(new Dimension(40, 20));
		add(lfgreen);
		add(fgreen);
		fgreen.setPreferredSize(new Dimension(40, 20));
		add(lfblue);
		add(fblue);
		fblue.setPreferredSize(new Dimension(40, 20));
	}

	@Override
	public void checkEditing() throws Exception {
		if (!EditorUtil.isInt(x1) ||
			!EditorUtil.isInt(y1) ||
			!EditorUtil.isInt(radius) ||
			!EditorUtil.isColor(rred) ||
			!EditorUtil.isColor(rgreen) ||
			!EditorUtil.isColor(rblue) ||
			!EditorUtil.isColor(fred) ||
			!EditorUtil.isColor(fgreen) ||
			!EditorUtil.isColor(fblue)) {
			throw new Exception("Invalid field");
		}
	}

	@Override
	public void acceptEditing() {
		int cx = EditorUtil.isEmpty(x1) ? fcircle.center.x : EditorUtil.getInt(x1);
		int cy = EditorUtil.isEmpty(y1) ? fcircle.center.y : EditorUtil.getInt(y1);
		int crad = EditorUtil.isEmpty(radius) ? (int) fcircle.radius : EditorUtil.getInt(radius);
		int nrred = EditorUtil.isEmpty(rred) ? fcircle.rimCol.getRed(): EditorUtil.getInt(rred);
		int nrgreen = EditorUtil.isEmpty(rgreen) ? fcircle.rimCol.getGreen() : EditorUtil.getInt(rgreen);
		int nrblue = EditorUtil.isEmpty(rblue) ? fcircle.rimCol.getBlue() : EditorUtil.getInt(rblue);
		int nfred = EditorUtil.isEmpty(fred) ? fcircle.getFillc().getRed() : EditorUtil.getInt(fred);
		int nfgreen = EditorUtil.isEmpty(fgreen) ? fcircle.getFillc().getGreen() : EditorUtil.getInt(fgreen);
		int nfblue = EditorUtil.isEmpty(fblue) ? fcircle.getFillc().getBlue() : EditorUtil.getInt(fblue);
		fcircle.setCenter(new Point(cx, cy));
		fcircle.setRadius(crad);
		fcircle.setRimCol(new Color(nrred, nrgreen, nrblue));
		fcircle.setFillc(new Color(nfred, nfgreen, nfblue));
		fcircle.fire();
	}

}
