package hr.fer.zemris.java.hw17.jvdraw.color;

import java.awt.Color;

/**
 * Provider of color, can register listeners
 * @author Fabijan
 *
 */
public interface IColorProvider {

	/**
	 * provide color
	 * @return
	 */
	public Color getCurrentColor();

	/**
	 * Register listener
	 * @param l
	 */
	public void addColorChangeListener(ColorChangeListener l);

	/**
	 * Yeet the listener straight to hell
	 * @param l
	 */
	public void removeColorChangeListener(ColorChangeListener l);

}
