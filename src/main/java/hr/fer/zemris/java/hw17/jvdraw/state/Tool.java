package hr.fer.zemris.java.hw17.jvdraw.state;

import java.awt.Graphics2D;
import java.awt.event.MouseEvent;

/**
 * Tool interface, a tool is used to generate and draw a geo object
 * based on the movement of the mouse
 * @author Fabijan
 *
 */
public interface Tool {

	/**
	 * mouse pressed event
	 * @param e
	 */
	public void mousePressed(MouseEvent e);

	/**
	 * mouse released event
	 * @param e
	 */
	public void mouseReleased(MouseEvent e);

	/**
	 * mouse clicked
	 * @param e
	 */
	public void mouseClicked(MouseEvent e);

	/**
	 * mouse moved
	 * @param e
	 */
	public void mouseMoved(MouseEvent e);

	/**
	 * mouse dragged
	 * @param e
	 */
	public void mouseDragged(MouseEvent e);

	/**
	 * Paint, I never used this one...
	 * @param g2d
	 */
	public void paint(Graphics2D g2d);

}
