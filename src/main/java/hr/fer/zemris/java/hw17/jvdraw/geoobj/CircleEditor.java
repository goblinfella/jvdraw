package hr.fer.zemris.java.hw17.jvdraw.geoobj;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.Point;

import javax.swing.JLabel;
import javax.swing.JTextField;

/**
 * Editor for circle
 * @author Fabijan
 *
 */
@SuppressWarnings("serial")
public class CircleEditor extends GeometricalObjectEditor {

	private Circle circle;
	
	private JLabel lx1 = new JLabel("x1");

	private JLabel ly1 = new JLabel("y1");
	
	private JLabel lradius = new JLabel("radius");

	private JLabel lrred = new JLabel("rim red");

	private JLabel lrgreen = new JLabel("rim green");

	private JLabel lrblue = new JLabel("rim blue");

	private JTextField x1 = new JTextField();

	private JTextField y1 = new JTextField();
	
	private JTextField radius = new JTextField();

	private JTextField rred = new JTextField();

	private JTextField rgreen = new JTextField();

	private JTextField rblue = new JTextField();
	/**
	 * @param circle
	 */
	public CircleEditor(Circle circle) {
		super();
		this.circle = circle;
		add(lx1);
		add(x1);
		x1.setPreferredSize(new Dimension(40, 20));
		add(ly1);
		add(y1);
		y1.setPreferredSize(new Dimension(40, 20));
		
		add(lradius);
		add(radius);
		radius.setPreferredSize(new Dimension(40, 20));

		add(lrred);
		add(rred);
		rred.setPreferredSize(new Dimension(40, 20));
		add(lrgreen);
		add(rgreen);
		rgreen.setPreferredSize(new Dimension(40, 20));
		add(lrblue);
		add(rblue);
		rblue.setPreferredSize(new Dimension(40, 20));
	}

	@Override
	public void checkEditing() throws Exception {
		if (!EditorUtil.isInt(x1) ||
			!EditorUtil.isInt(y1) ||
			!EditorUtil.isInt(radius) ||
			!EditorUtil.isColor(rred) ||
			!EditorUtil.isColor(rgreen) ||
			!EditorUtil.isColor(rblue)) {
			throw new Exception("Invalid field");
		}
	}

	@Override
	public void acceptEditing() {
		int cx = EditorUtil.isEmpty(x1) ? circle.center.x : EditorUtil.getInt(x1);
		int cy = EditorUtil.isEmpty(y1) ? circle.center.y : EditorUtil.getInt(y1);
		int crad = EditorUtil.isEmpty(radius) ? (int) circle.radius : EditorUtil.getInt(radius);
		int nrred = EditorUtil.isEmpty(rred) ? circle.rimCol.getRed(): EditorUtil.getInt(rred);
		int nrgreen = EditorUtil.isEmpty(rgreen) ? circle.rimCol.getGreen() : EditorUtil.getInt(rgreen);
		int nrblue = EditorUtil.isEmpty(rblue) ? circle.rimCol.getBlue() : EditorUtil.getInt(rblue);
		circle.setCenter(new Point(cx, cy));
		circle.setRadius(crad);
		circle.setRimCol(new Color(nrred, nrgreen, nrblue));
		circle.fire();
	}

}
