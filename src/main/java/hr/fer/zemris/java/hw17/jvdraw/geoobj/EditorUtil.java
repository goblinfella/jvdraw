package hr.fer.zemris.java.hw17.jvdraw.geoobj;

import javax.swing.JTextField;

/**
 * Cool little utility class
 * @author Fabijan
 *
 */
public class EditorUtil {
	
	/**
	 * Is the text field ""
	 * @param t
	 * @return
	 */
	static boolean isEmpty(JTextField t) {
		return t.getText().isEmpty();
	}
	
	/**
	 * Is there a parasble int in the text fiel?
	 * @param t
	 * @return
	 */
	static boolean isInt(JTextField t) {
		if (isEmpty(t)) return true;
		getInt(t);
		return true;
	}
	
	/**
	 * Is it an rgb component?
	 * @param t
	 * @return
	 */
	static boolean isColor(JTextField t) {
		if (isEmpty(t)) return true;
		int c = getInt(t);
		return c < 256 && c >= 0;
	}
	
	/**
	 * Get that int
	 * @param t
	 * @return
	 */
	static int getInt(JTextField t) {
		return Integer.parseInt(t.getText());
	}

}
