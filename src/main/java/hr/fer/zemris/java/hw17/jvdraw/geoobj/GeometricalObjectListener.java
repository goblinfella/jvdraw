package hr.fer.zemris.java.hw17.jvdraw.geoobj;

/**
 * Listener of object
 * @author Fabijan
 *
 */
public interface GeometricalObjectListener {
	
	/**
	 * The object has changed
	 * @param o
	 */
	public void geometricalObjectChanged(GeometricalObject o);

}
