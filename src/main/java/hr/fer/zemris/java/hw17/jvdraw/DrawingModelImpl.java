package hr.fer.zemris.java.hw17.jvdraw;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;
import java.util.Objects;

import hr.fer.zemris.java.hw17.jvdraw.geoobj.GeometricalObject;
import hr.fer.zemris.java.hw17.jvdraw.geoobj.GeometricalObjectListener;

/**
 * Concrete implementation of drawing model
 * @author Fabijan
 *
 */
public class DrawingModelImpl implements DrawingModel, GeometricalObjectListener{
	
	/**
	 * Objects in model
	 */
	private List<GeometricalObject> geobjs = new ArrayList<>();
	
	/**
	 * modifed flag
	 */
	private boolean modified;
	
	/**
	 * listeners
	 */
	private List<DrawingModelListener> listeners = new LinkedList<>();

	@Override
	public int getSize() {
		return geobjs.size();
	}

	@Override
	public GeometricalObject getObject(int index) {
		return geobjs.get(index);
	}

	@Override
	public void add(GeometricalObject object) {
		object.addGeometricalObjectListener(this);
		geobjs.add(object);
		setModified(true);
		listeners.forEach( l -> l.objectsAdded(this, listeners.size() - 1, listeners.size() - 1));
	}

	@Override
	public void remove(GeometricalObject object) {
		int index = geobjs.indexOf(object);
		if (index == -1) {
			return;
		}
		geobjs.remove(object);
		object.removeGeometricalObjectListener(this);
		setModified(true);
		listeners.forEach( l -> l.objectsAdded(this, index, index));
	}

	@Override
	public void changeOrder(GeometricalObject object, int offset) {
		int index = geobjs.indexOf(object);
		int newIndex = index + offset;
		if (newIndex < 0 || newIndex >= getSize()) return;
		if (geobjs.remove(object)) {
			geobjs.add(newIndex, object);
		};
		setModified(true);
		listeners.forEach( l -> l.objectsAdded(this, index, newIndex));
	}

	@Override
	public int indexOf(GeometricalObject object) {
		return geobjs.indexOf(object);
	}

	@Override
	public void clear() {
		geobjs.clear();
		setModified(true);
	}

	@Override
	public void clearModifiedFlag() {
		setModified(false);;
	}

	@Override
	public boolean isModified() {
		return modified;
	}
	
	private void setModified(boolean b) {
		// TODO notify
		modified = b;
	}

	@Override
	public void addDrawingModelListener(DrawingModelListener l) {
		Objects.requireNonNull(l);
		listeners = new LinkedList<DrawingModelListener>(listeners);
		listeners.add(l);
	}

	@Override
	public void removeDrawingModelListener(DrawingModelListener l) {
		Objects.requireNonNull(l);
		listeners = new LinkedList<DrawingModelListener>(listeners);
		listeners.remove(l);
	}

	@Override
	public void geometricalObjectChanged(GeometricalObject o) {
		setModified(true);
		int index = geobjs.indexOf(o);
		listeners.forEach( l -> l.objectsAdded(this, index, index));
	}

}
